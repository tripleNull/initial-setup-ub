# initial-setup-ub

Some scripts for installing packages / making common changes during the first boot of Ubuntu 18.10. Changes are logged to timestamped .log files in your home folder.

## Usage Instructions
Create a list commands in $HOME/.config/isu/commands with the commands for each item, one command per line.

### Included functions for the commands file:
	repo_get <package_name>
		Installs the specified package using apt
	install_subl
		Adds the Sublime Text repo and installs the sublime-text package using repo_get
	isu_quit
		Exits the menu

Create a comma delimited list of descriptions for each of the commands in $HOME/.config/isu/descriptions.
Example files have been included in this repo. 

### Execute the script
	chmod +x ./install-script.sh
	./install-script.sh

## Example Configuration Files set up a menu to allow the user to install the following packages and generate an SSH Key
	1. Sublime Text
	2. KeePassXC
	3. FileZilla
	4. Neofetch
	5. Nvidia Drivers
	
	The script will automatically creates a bin directory under $HOME, which is automatically added to the $PATH

