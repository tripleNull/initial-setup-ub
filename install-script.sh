#!/bin/bash

# Set up menu variables
mloop=1
tempn=10000
command_file="$HOME/.config/isu/commands"
description_file="$HOME/.config/isu/descriptions"

# Read in the list of commands from the user
readarray isu_commands < "$command_file"

# Read in the descriptions from the file, this will be one line
while IFS='' read -r line || [[ -n "$line" ]]; do
    isu_description_file="${line}"
done < "$description_file"

n=0

# Split the line of descriptions by ,
while IFS=',' read -ra ADDR; do
	for m in "${ADDR[@]}"; do
	    isu_description[$n]=$m
	    n=$((n+1))
	done
done <<< "$isu_description_file"

# Initialize checklist array
for iloop in ${!isu_description[@]}; do
	isu_checklist[$iloop]="[X]"
done

# Functions
# get current time to append to the log file name
isu_get_time(){
	isu_timestamp=`date "+%y%m%d_%H%M%S"`
}

# get current time for log entries
isu_get_log_time(){
	isu_timestamp_log=`date "+%m/%d/%y %H:%M:%S"`
}

# append a timestamp and the first parameter into the log file
isu_log_text(){
	isu_get_log_time
	echo "$isu_timestamp_log -- $1" >> $isu_log_file
}

# install sublime text
install_subl(){
	# Sublime Text
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
	sudo apt-get install apt-transport-https
	echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
	sudo apt-get update
	repo_get "sublime-text"
}

# Grab package specified in the first parameter from the repos
repo_get(){
	isu_log_text "Installing Package: $1"
	sudo apt-get install $1 -yy >> $isu_log_file
}

# Quit
isu_quit(){
	isu_log_text "Exiting"
	exit 0
}

# Display the menu and prompt the user for input - TODO: Need to catch bad user input
isu_menu(){
	# Update options to include completed string before displaying the options again
	for oloop in ${!isu_description[@]}; do

		if [[ "$tempn" = "$oloop" ]]; then
			isu_checklist[$oloop]="[completed]"
		fi

		options[$oloop]="${isu_description[$oloop]} ${isu_checklist[$oloop]}"

	done

	clear
	echo "Please choose a package to install or generate an SSH key: "

	# Display options
	for i in "${!options[@]}"; do
		j=$((i + 1))
		echo "$j - ${options[i]}"
	done

	# Prompt user 
	read -p "Selection: " sel

	# Handle indexing starting with zero
	tempn=$((sel - 1))

	# Execute selection
	${isu_commands[$tempn]}
}

# Set up logging
isu_get_time
isu_log_file="$HOME/isu_log_$isu_timestamp.log"
isu_log_text "Starting install script"

# Update from the repos
sudo apt-get update && sudo apt-get upgrade

# add in a bin directory under home, if it does not already exist
if [[ -d $HOME/bin ]] 
then
	isu_log_text "bin already exists under home"
else
	isu_log_text "Creating bin directory under home"
	mkdir $HOME/bin
fi

# Menu loop
while [ $mloop -ge 1 ]
do
	echo "Menu loop"
	isu_menu
done
